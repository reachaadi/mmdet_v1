#!/bin/bash

# Usage: sh sample/make_bccd_cocojson.sh

for split in trainval test
do
    python voc2coco.py --ann_dir data/VOC2007/Annotations --ann_ids data/VOC2007/ImageSets/Main/trainval.txt --labels data/labels.txt --output data/outputs/trainval.json --ext xml 
    done